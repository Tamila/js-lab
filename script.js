var tiles = ["🀱", "🀲", "🀳", "🀴", "🀵", "🀶", "🀷", "🀸", "🀹", "🀺", "🀻", "🀼", "🀽", "🀾", 
			 "🀿", "🁀", "🁁", "🁂", "🁃", "🁄", "🁅", "🁆", "🁇", "🁈", "🁉", "🁊", "🁋", "🁌",
			 "🁍", "🁎", "🁏", "🁐", "🁑", "🁒", "🁓", "🁔", "🁕", "🁖", "🁗", "🁘", "🁙", "🁚",
			 "🁛", "🁜", "🁝", "🁞", "🁟", "🁠", "🁡"]
var angle = 0;

function rotateDomino(d) {
	angle += d;
    var s = "rotate(" + angle + "deg)";
    document.getElementsByClassName("tile")[0].style.WebkitTransform = s;
}

$(document).on('click', '#left',  function(){
	rotateDomino(90) 
});

$(document).on('click', '#right',  function(){
	rotateDomino(-90) 
});

$(document).on('click', '#random', function(){
	document.getElementsByClassName("tile")[0].innerHTML = tiles[Math.floor(Math.random() * 49)];
})

var interval = setInterval(intervalfunction, 25)

function intervalfunction() {
	rotateDomino(0.25);
	var size = document.getElementById("myRange").value;
	document.getElementsByClassName("tile")[0].style.fontSize = size+"px";
}